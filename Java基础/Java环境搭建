
><font size=4>**写在前面**</font>
> &emsp;&emsp;你们好，我是小庄。很高兴能和你们一起学习Java。如果您对Java感兴趣的话可关注我的动态.
>  &emsp;&emsp;`写博文是一种习惯，在这过程中能够梳理和巩固知识。`

## 1、JDK安装
安装建议：推荐安装JDK1.8版本

安装步骤：

 1. [点击JDK1.8版本下载地址进入](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
链接：https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
 2. 选择下载版本
 ![选择版本](https://img-blog.csdnimg.cn/20210705214411463.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
 3. 双击启动下载好的exe文件
 ![安装界面](https://img-blog.csdnimg.cn/2021070523173162.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)

 4. 点击下一步然后点击更改选择安装路径一直到安装完毕
 ![安装图片](https://img-blog.csdnimg.cn/20210705231908664.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
## 2、配置JAVA_HOME环境变量
步骤如下：

 1. 右击“此电脑” -->  **选择属性**
![此电脑](https://img-blog.csdnimg.cn/20210705232426880.png#pic_center)
 2. 选择高级系统设置 --> 进入系统属性
![高级系统设置](https://img-blog.csdnimg.cn/20210705232751418.png#pic_center)
 3. 选择环境变量
 ![选择环境变量](https://img-blog.csdnimg.cn/20210705232935213.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)

 4. 在系统变量新建JAVA_HOME，设置值为**JDK安装路径**，例如：我的jdk安装目录如下：
 ![环境变量安装](https://img-blog.csdnimg.cn/20210705233200375.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
 5. 在系统变量原有的Path点击编辑按钮，并在弹窗点击新建进行追加新内容
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210705233532793.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210705234619406.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
 6. 输入：`%JAVA_HOME%\bin`
 7. 依次按确定退出
 8. 进入cmd
 Win+R(win键在键盘左下角,ctrl和alt中间是个图标),   输入cmd点击确定
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210705235030755.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)

 9. 输入命令:`java -version`然后回车
 ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210705235246342.png#pic_center)
## 3、Eclispe下载安装
[点击进入下载Eclispe](https://www.eclipse.org/downloads/packages/release/)
链接：https://www.eclipse.org/downloads/packages/release/
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021070600031110.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
选择自己的版本进行下载

对压缩包进行解压到指定路径即可使用
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021070600064399.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
启动Eclispe后选择工作空间存放路径
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210706001114882.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDcxNTY0Mw==,size_16,color_FFFFFF,t_70#pic_center)
<hr>
环境搭建完成
